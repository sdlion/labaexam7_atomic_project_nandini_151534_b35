<?php
namespace App\Birthday;
use App\Message\Message;
use App\Model\database as db;
use App\Utility\Utility;

//require_once("../../../../vendor/autoload.php");
class Birthday extends db
{
    public $id;
    public $name;
    public $dob;

    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data = Null)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];

        }
        if (array_key_exists('name', $data)) {
            $this->name= $data['name'];

        }
        if (array_key_exists('dob', $data)) {
            $this->dob = $data['dob'];

        }

    }
    public function store(){
        $arrData=array($this->name,$this->dob);

        $sql= "Insert INTO birthday(name,dob) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("Sucess!data has been inserted sucessfully");
        else
            Message::setMessage("Failure!data has not been inserted sucessfully");

        Utility::redirect('create.php');
    }// end of store method

}