<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Upoload User Profile Picture</title>
    <!-------Including jQuery from Google ------>
    <script src="../../../resource/assets/assets_propic/js/jquery.min.js"></script>
    <script src="../../../resource/assets/assets_propic/js/script.js"></script>
    <!------- Including CSS File ------>
    <link rel="stylesheet" type="text/css" href="../../../resource/assets/assets_propic/css/style.css">
<body>
<div id="maindiv">

    <div id="formdiv">
        <h2>Multiple Image Upload Form</h2>

        <form enctype="multipart/form-data" action="store.php" method="post">            Browse Picture
            <div class="form-group">
                <label class="sr-only" >Username</label>
                <input type="text" name="name" placeholder="Username..." >
            </div>

            <div id="filediv">
                <input type ="file" name="image" id= "fileToUpload">
            </div>
            <input type ="submit" value="Upload image" name="submit" />


        </form>
        <!------- Including PHP Script here ------>

    </div>
</div>
</body>
</html>